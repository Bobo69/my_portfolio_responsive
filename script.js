// ScrollReveal().reveal('.banner ul', { delay: 2000 });

ScrollReveal().reveal('.dot', { delay: 600, interval: 300, reset: true});

ScrollReveal().reveal('.banner h2', { delay: 2000, reset: true })

ScrollReveal().reveal('.portrait-description', { delay: 600, reset: true })

ScrollReveal().reveal('.card', { delay: 400, interval: 300, reset: true })

ScrollReveal().reveal('i', { delay: 400, interval: 300, reset: true })

ScrollReveal().reveal('.picture', { distance: '90px', origin: 'left', delay: 500, reset: true  });